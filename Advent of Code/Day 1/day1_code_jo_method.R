setwd("/source/advent-of-code-jo/Advent of Code/Day 1")

depths <- read.csv("/source/advent-of-code-jo/Advent of Code/Day 1/Data/AoC Depths.csv")

View(depths)

library(data.table)
library(dplyr)
setDT(depths)

#Part 1
increases <- depths[, sum(diff(Depth) > 0)]
increases

#Part 2
library(zoo)
roll <- 3

three_part_rolls <- rollapply(depths$Depth, 2*roll-1, function(x) max(rollsum(x, roll)), partial = TRUE)

df_rolls <- data.frame(three_part_rolls)
setDT(df_rolls)

View(df_rolls)

df_rolls2 <- df_rolls[three_part_rolls > 0]

roll_increases <- df_rolls2[, sum(diff(three_part_rolls) > 0)]
roll_increases
